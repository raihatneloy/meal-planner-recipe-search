import { getAPIData } from "../data";

/**
 * Fetches meal plans from Edamam.
 * @param {Object} data Meal data
 * @return {Promise} Promise object for the async fetch response.
 */
const getPlan = data => {
  const { ID, KEY, URL } = getAPIData();
  const queries = buildQuery(data, ID, KEY);

  // sends all API queries, and resturns an array of Promises.
  const promises = queries.map(query =>
    fetch(encodeURI(URL + query)).then(response => response.json())
  );

  // Return a Promise comprising of the Promise array to handle a single fulfillment.
  return Promise.all(promises);
};

/**
 * Builds a query string for meal API.
 * @param {Object} data Object containing the data about the meal.
 * @param {string} id Edamam API identifier.
 * @param {string} key Edamam Unique API key for usage.
 * @returns {string} The parsed quesy string.
 */
const buildQuery = (data, id, key) => {
  // Calories per meal.
  // Parse int will remove any fractions. We add 0.5 to round first, then parse to
  // an integer.
  const caloriesPerMeal = {
    min: parseInt(data.calories.min / data.meals.length + 0.5, 10),
    max: parseInt(data.calories.max / data.meals.length + 0.5, 10)
  };

  let healthOptions = Object.keys(data.health).map(key => `health=${key}`);
  healthOptions.push("");

  // Map an array of url query strings.
  return data.meals.map(
    meal =>
      `q=${meal}&` +
      `app_id=${id}&` +
      `app_key=${key}&` +
      `to=${parseInt(data.plan, 10)}&` +
      `${healthOptions.length > 1 ? healthOptions.join("&") : ""}` +
      `diet=${data.diet}&` +
      `calories=${caloriesPerMeal.min}-${caloriesPerMeal.max}`
  );
};

export default getPlan;
