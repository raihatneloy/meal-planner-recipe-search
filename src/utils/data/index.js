/** Get data from formatted JSON file */
const surveyData = require("./data.json");
const getSurveyData = () => surveyData;

/** Safely get API keys from environment variables
 * (so they're not stored in source) */
const API = {
  ID: process.env.EDAMAM_ID,
  KEY: process.env.EDAMAM_KEY,
  URL: process.env.EDAMAM_URL
};
const getAPIData = () => API;

export { getSurveyData, getAPIData };
