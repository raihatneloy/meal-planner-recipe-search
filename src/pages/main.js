import ReactDOM from "react-dom";
import React, { Component } from "react";
import { Provider } from "react-redux";
import { HashRouter as Router, Route } from "react-router-dom";
import "font-awesome/css/font-awesome.css";

import { Store } from "@redux";
import { Locale } from "@locale";

import Landing from "@components/page/Landing";
import Survey from "@components/page/Survey";
import Plan from "@components/page/Plan";
import Footer from "@components/shared/Footer";
import Search from "@components/page/Search";
import Favorites from "@components/page/Favorites";

import "@themes/style.scss";

// Initialize Locale
Locale.init();

class Application extends Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={Landing} />
        <Route path="/survey" component={Survey} />
        <Route path="/meal-plan" component={Plan} />
        <Route path="/search/:searchText" component={Search} />
        <Route path="/favorites" component={Favorites} />
        <Footer />
      </div>
    );
  }
}

ReactDOM.render(
  <Provider store={Store}>
    <Router>
      <Application />
    </Router>
  </Provider>,
  document.getElementById("app")
);
