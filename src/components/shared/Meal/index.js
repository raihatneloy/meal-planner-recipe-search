import React from 'react';
import Proptypes from 'prop-types';
import Tag from '../Tag';
import './Meal.css';

class Meal extends React.Component {
  openModal() {
    $("#" + this.props.element).modal('show');
  }
  
  isFavorite(props){
    var favoritesList = localStorage.getItem('favorites');

    if (favoritesList === null)
      favoritesList = "[]";

    favoritesList = JSON.parse(favoritesList);

    for (var i=0;i<favoritesList.length;i++){
      if (favoritesList[i].url === props.url)
        return true;
    }

    return false;
  }

  addToFavorite(props, favorite){
    var favoritesList = localStorage.getItem('favorites');

    if (favoritesList === null)
      favoritesList = "[]";

    favoritesList = JSON.parse(favoritesList);

    if (favorite === true){
      var index = -1;
      for (var i=0;i<favoritesList.length;i++){
        if (props.url === favoritesList[i].url){
          index = i;
          break;
        }
      }
      favoritesList.splice(index, 1);
    } else {
      favoritesList.push(props);
    }
    localStorage.setItem('favorites', JSON.stringify(favoritesList));

    if ('favoritePage' in props){
      props.favoritePage();
    }

    this.forceUpdate();
  }

  render() {
    var props = this.props;
    var favorite = this.isFavorite(props);
    return (
    <div className="Meal">
      <div className="Meal__head">{props.type}</div>
      <div className="Meal__content">
        <a  href={props.url} target="_blank">
          <div className="Meal__content__img">
            <img src={props.imgSrc} alt="Unavailable"/>
          </div>
        </a>
        <div className="Meal__content__desc">
            <h2 className="Meal__content__desc--heading">
              <a style={{color: "#34495e"}}  href={props.url} target="_blank">
                {props.heading}
              </a>
            <span style={{float: "right"}}
              onClick={() => this.openModal()}
            ><i class="fa fa-ellipsis-v" />
            </span></h2>
            <h4 className="Meal__content__desc--source">
              {props.source}
              {'searchResult' in props && (
                <span style={{float: "right"}}
                  onClick={() => this.addToFavorite(props, favorite)}
                >
                  {favorite === true && <i class="fa fa-heart" style={{color: "red"}}/>}
                  {favorite === false && <i class="fa fa-heart" />}
                </span>
              )}
            </h4>
        </div>
        <div className="Meal__content__labels">
          {
            props.tags.map((tag,i) => <Tag icon={props.tags.icon} name={tag} key={`Tag__${i}`} />)
          }
        </div>
        <div class="ui modal" id={props.element}>
          <div class="header">Ingredients for {this.props.heading}</div>
          <div class="content">
            <ul>
              {
                this.props.ingredients.map((text) => {
                  return (
                    <li>{text}</li>
                  );
                })
              }
            </ul>
          </div>
        </div>
      </div>
    </div>
  )};
} 

Meal.propTypes = {
  element: Proptypes.string,
  url: Proptypes.string,
  type: Proptypes.string,
  imgSrc: Proptypes.string,
  heading: Proptypes.string,
  source: Proptypes.string,
  tags: Proptypes.array,
}
export default Meal;
