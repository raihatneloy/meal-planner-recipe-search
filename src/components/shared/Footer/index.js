import React from 'react';
import './Footer.css';

const Footer = () => (
  <div>
    <br/>
    <div className="Footer">
      <p>
        Creado con usando
        <a href="https://www.edamam.com/" target="_blank" rel="noopener noreferrer" className="Footer__link"> Edamam API</a>
      </p>
    </div>
  </div>
);

export default Footer;
