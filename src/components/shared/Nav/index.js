import React from 'react';
import Button from '../Button';
import Input from '../Form/Input';
import './Nav.css';

const submitSearch = (text) => {
  if (text !== '')
    window.location = "/#/search/" + text;
};

const Nav = () => (
  <div className="Nav">
    <Button className="Nav__button" link={true} path="/" type="transparent">Home</Button>
    <Button className="Nav__button" link={true} path="/survey" type="transparent">Survey</Button>
    <Button className="Nav__button" link={true} path="/favorites" type="transparent">Favorite Recipes</Button>
    <Input searchMethod={submitSearch}/>
  </div>
);

export default Nav;
