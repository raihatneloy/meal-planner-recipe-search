import React from 'react';

import Button from '../../Button';

export default class Input extends React.Component{
  state = {
    text: ""
  }
  render() {
    return (
      <div style={{float: "right"}}>
      <input
        value={this.state.text}
        onChange={
          (e) => this.setState({text: e.target.value})
        }
        placeholder="Search Recipe"
        style={{
          borderColor: "#34495e",
        }}
      />
      <Button onClick={() => this.props.searchMethod(this.state.text)}><i class="fa fa-search"/></Button>
      </div>
    )
  }
}