import React from 'react';
import Nav from "@components/shared/Nav";
import Meal from "@components/shared/Meal";

export default class Search extends React.Component{
  state = {
    recipes: [],
    searchText: this.props.match.params.searchText,
  }
  fetchData(){
    var api='https://api.edamam.com/search?q=' + this.state.searchText + '&app_id=c3e8b574&app_key=599030435b2e72e3548121fd40900bac&to=100';

    fetch(api)
      .then(response => response.json())
      .then(response => {
        this.setState({
          recipes: response['hits']
        })
      })
  }
  componentWillReceiveProps(newProps){
    this.setState({
      searchText: newProps.match.params.searchText,
    }, this.fetchData);
  }
  componentDidMount(){
    this.fetchData();
  }
  render() {
    return (
      <div>
        <Nav />
        <div style={{
          maxWidth: "1000px",
          width: "80%",
          marginLeft: "50%",
          transform: "translate(-50%)",
        }}>
        {this.state.recipes.map((element, i) => {
          return (
            <div
              style={{
                width: "45%",
                float: "left",
              }}
            >
            <Meal
              type={element.recipe.label}
              imgSrc={element.recipe.image}
              heading={element.recipe.label}
              source={element.recipe.source}
              tags={[...element.recipe.dietLabels, ...element.recipe.healthLabels].slice(0, 4)}
              url={element.recipe.url}
              element={i}
              searchResult="true"
              ingredients={element.recipe.ingredientLines}
              key={`Meal__Search__${i}`}
            />
            {/*<img src={element['recipe']['image']}/>*/}
            </div>
          );
        })}
        </div>
      </div>
    )
  }
}
