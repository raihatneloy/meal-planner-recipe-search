import React from 'react';
import Nav from "@components/shared/Nav";
import Meal from "@components/shared/Meal";

export default class Favorites extends React.Component{
  state = {
    favorites: [],
  }

  componentDidMount(){
    var favoritesList = localStorage.getItem('favorites');

    if (favoritesList === null){
      favoritesList = [];
    } else {
      favoritesList = JSON.parse(favoritesList);
    }

    this.setState({
      favorites: favoritesList,
    })
  }

  reloadFavorites(){
    this.setState({
      favorites: JSON.parse(localStorage.getItem('favorites')),
    })
  }

  render() {
    return (
      <div>
      <Nav />
      <div style={{
        maxWidth: "1000px",
        width: "80%",
        marginLeft: "50%",
        transform: "translate(-50%)",
      }}>
      <h2>Your Favorite Recipes</h2>
      {this.state.favorites.map((element) => {
          console.log(element);
          return (
            <div
              style={{
                width: "45%",
                float: "left",
              }}
            >
            <Meal
              type={element.type}
              imgSrc={element.imgSrc}
              heading={element.heading}
              source={element.source}
              tags={element.tags}
              url={element.url}
              element={element.element}
              searchResult="true"
              favoritePage={this.reloadFavorites.bind(this)}
              ingredients={element.ingredients}
            />
            {/*<img src={element['recipe']['image']}/>*/}
            </div>
          );
      })}
      </div>
      </div>
    );
  }
}
