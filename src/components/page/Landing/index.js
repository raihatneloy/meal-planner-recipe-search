import React from "react";
import "./Landing.css";
import Button from "@components/shared/Button";

const Landing = props => (
  <div className="Landing">
    <div className="Landing__banner">
      <h1 className="Landing__banner__heading">
        Facil planificación de comidas
      </h1>
    </div>
    <div className="Landing__data">
      <div className="Landing__data__content">
        <h1>Suena como usted?</h1>
        <ul>
          <li>
            Pierde mucho tiempo pensando en lo que debería preparar para la cena
            de hoy.
          </li>
          <li>Quiere ver que come pero no sabe como?</li>
          <li>No es bueno en la planificación previa de las comidas.</li>
          <li>Quiere rastrear su peso y la ingesta de calorías</li>
        </ul>
        <Button
          type="accent"
          link={true}
          path="/survey"
          className="Landing__data__button"
        >
          Vamos a encontrar ese plan de comidas
        </Button>
      </div>
    </div>
  </div>
);

export default Landing;
